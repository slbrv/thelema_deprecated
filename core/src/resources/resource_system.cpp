#include <string>
#include "thelema/resources/resource_system.h"

#define MEDIA_DIR "./media"
#define TEXTS_DIR "texts"
#define SHADERS_DIR "shaders"
#define SOUNDS_DIR "sounds"
#define TEXTURES_DIR "textures"


namespace Thelema {
    bool Thelema::ResourceSystem::init() {
        const static fs::path resourceRootDir(MEDIA_DIR);
        bool isSuccess = true;
        // TODO(G): сделать компонент "Resource" и пройтись по всем компонентам этого типа вместо хардкода.
        isSuccess &= loadResource<Text>(resourceRootDir / fs::path(TEXTS_DIR));
        isSuccess &= loadResource<AbstractShader>(resourceRootDir / fs::path(SHADERS_DIR));
        isSuccess &= loadResource<Sound>(resourceRootDir / fs::path(SOUNDS_DIR));
        isSuccess &= loadResource<Texture>(resourceRootDir / fs::path(TEXTURES_DIR));
        return isSuccess;
    }

    bool Thelema::ResourceSystem::start() {
        return true;
    }

    bool Thelema::ResourceSystem::update() {
        return true;
    }

} // Thelema

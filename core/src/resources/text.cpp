#include <fstream>
#include "thelema/resources/text.h"

// Defines
#define TXT_EXTENSION ".txt"

namespace Thelema {
    const char *Text::getCStr() const{
        return _text.c_str();
    }

    std::string &Text::getString(){
        return _text;
    }

    bool Text::loadFromFile(fs::path &path) {
        std::ifstream file(path.string());
        if (!file.is_open()) { return false; }
        file.seekg(0, std::ios::end);
        auto len = file.tellg();
        _text.resize(len);
        file.seekg(0);
        file.read(&_text[0], len);
        file.close();
        return true;
    }

    const std::vector<std::string> &Text::getResourcesExtensions() {
        const static std::vector<std::string> extensions{TXT_EXTENSION};
        return extensions;
    }
};

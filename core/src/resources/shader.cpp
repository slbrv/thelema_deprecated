// Include
#include <experimental/filesystem>
#include <cstring>
#include <map>
#include <thelema/exceptions/systems_exceptions.h>
#include "thelema/resources/abstract_shader.h"

// Defines
#define VERTEX_EXTENSION ".shader_vertex"
#define FRAGMENT_EXTENSION ".shader_fragment"
#define GEOMETRY_EXTENSION ".shader_geometry"

namespace fs = std::experimental::filesystem;

namespace Thelema {

    std::unique_ptr<char[]> AbstractShader::getShaderText(const fs::path &path) {
        Text text;
        if (!text.loadFromFile(path)) {
            throw ResourceLoadException(path.string());
        }
        auto shaderText = std::unique_ptr<char[]>(new char[strlen(text.getCStr())]);
        strcpy(shaderText.get(), text.getCStr());
        return std::move(shaderText);
    }

    const std::vector<std::string> &AbstractShader::getResourcesExtensions() {
        const static std::vector<std::string> extensions{VERTEX_EXTENSION, FRAGMENT_EXTENSION, GEOMETRY_EXTENSION};
        return extensions;
    }

    AbstractShader::AbstractShader() : _shader(0), _type(None), _shaderText() {
    }

    bool AbstractShader::loadFromFile(fs::path &path) {
        const static std::map<fs::path, Type> extensionToTypeMap{{VERTEX_EXTENSION,   Vertex},
                                                                 {FRAGMENT_EXTENSION, Fragment},
                                                                 {GEOMETRY_EXTENSION, Geometry}};
        auto it = extensionToTypeMap.find(path.extension());
        if (it == extensionToTypeMap.end()) {
            return false;
        }
        _type = it->second;
        try {
            _shaderText = getShaderText(path);
        } catch (EngineException &e) {
            // TODO: Log
            return false;
        }
        return true;
    }
}
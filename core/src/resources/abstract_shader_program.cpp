#include <thelema/resources/abstract_shader_program.h>

#define SHADER_PROGRAM_EXTENSION

const std::vector<std::string> &Thelema::AbstractShaderProgram::getResourcesExtensions() {
    const static std::vector<std::string> extensions{SHADER_PROGRAM_EXTENSION};
    return extensions;
}

Thelema::AbstractShaderProgram::AbstractShaderProgram() : IResource(), _shaderProgram(0){

}

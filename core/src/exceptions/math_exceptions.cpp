#include "thelema/exceptions/math_exceptions.h"

namespace Thelema {
    OutOfRange::OutOfRange(size_t i, size_t j) : EngineException(
        "Indexes (" + std::to_string(i) + ", " + std::to_string(j) + ") are out of range") {}

    SingularMatrix::SingularMatrix() : EngineException("Singular matrix") {}

}  // Thelema

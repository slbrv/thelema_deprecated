#include "thelema/exceptions/base_exception.h"

namespace Thelema {
    EngineException::EngineException(std::string message) : _message(std::move(message)) {}

    const char *EngineException::what() const noexcept {
        return _message.c_str();
    }


} // Thelema
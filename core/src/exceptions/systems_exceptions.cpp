#include <thelema/exceptions/systems_exceptions.h>

namespace Thelema {
    ResourceLoadException::ResourceLoadException(std::string message) : EngineException("Can't load resource: " + message) {
    }

    ResourceGetException::ResourceGetException(std::string message) : EngineException("Resource not found: " + message) {

    }
}  // Thelema


#include <thelema/math/vector/vec.h>

template<class T, size_t N>
Thelema::Vec<T, N>::Vec() {
    vector_size=0;
}

template<class T, size_t N>
Thelema::Vec<T, N>::Vec(Vec &rhs) {
    vector_size=rhs.vector_size;
    elements=rhs.elements;
}

template<class T, size_t N>
Thelema::Vec<T, N> Thelema::Vec<T, N>::operator=(Thelema::Vec<T,N> rhs) {
    vector_size=rhs.vector_size;
    elements=rhs.elements;
    return this;
}


template<class T, size_t N>
T *Thelema::Vec<T, N>::GetVec() const {
    return elements;
}

template<class T, size_t N>
size_t Thelema::Vec<T, N>::GetSize() const {
    return vector_size;
}

template<class T, size_t N>
T Thelema::Vec<T, N>::operator()(size_t i) const {
    if (i>vector_size) throw "Out of vector range";
    return elements[i];
}

template<class T, size_t N>
T *Thelema::Vec<T, N>::operator()(size_t i) {
    if (i>vector_size) throw "Out of vector range";
    return &elements[i];
}

template<class T, size_t N>
Thelema::Vec<T,N> Thelema::Vec<T, N>::operator+(const Thelema::Vec<T,N> rhs) const {
    for (int i=0;i<N;++i){
        elements[i]+=rhs.elements[i];
    }
    return this;
}

template<class T, size_t N>
Thelema::Vec<T,N> Thelema::Vec<T, N>::operator-(const Thelema::Vec<T,N> rhs) const {
    for (int i=0;i<N;++i){
        elements[i]-=rhs.elements[i];
    }
    return this;
}

template<class T, size_t N>
Thelema::Vec<T,N> Thelema::Vec<T, N>::operator*(const Thelema::Vec<T,N> rhs) const {
    for (int i=0;i<N;++i){
        elements[i]*=rhs.elements[i];
    }
    return this;
}

template<class T, size_t N>
Thelema::Vec<T,N> Thelema::Vec<T, N>::operator/(const Thelema::Vec<T,N> rhs) const {
    for (int i=0;i<N;++i){
        elements[i]/=rhs.elements[i];
    }
    return this;
}

template<class T, size_t N>
Thelema::Vec<T,N> Thelema::Vec<T, N>::operator+(const float rhs) const {
    for (int i=0;i<N;++i){
        elements[i]+=rhs;
    }
    return this;
}

template<class T, size_t N>
Thelema::Vec<T,N> Thelema::Vec<T, N>::operator-(const float rhs) const {
    for (int i=0;i<N;++i){
        elements[i]-=rhs;
    }
    return this;
}

template<class T, size_t N>
Thelema::Vec<T,N> Thelema::Vec<T, N>::operator*(const float rhs) const {
    for (int i=0;i<N;++i){
        elements[i]*=rhs;
    }
    return this;
}

template<class T, size_t N>
Thelema::Vec<T,N> Thelema::Vec<T, N>::operator/(const float rhs) const {
    for (int i=0;i<N;++i){
        elements[i]/=rhs;
    }
    return this;
}

template<class T, size_t N>
float Thelema::Vec<T, N>::length() const {
    return sqrt(dot(this, this));
}

template<class T, size_t N>
float Thelema::Vec<T, N>::dot(Thelema::Vec<T,N> a, Thelema::Vec<T,N> b) const {
    if (N>0){
        float result=0;
        for (int i=0;i< N;i++){
            result+=a[i]*b[i];
        }
    } else throw "Vector is empty";

}

template<class T, size_t N>
float Thelema::Vec<T, N>::distance(Thelema::Vec<T,N> b) const {
    return (this-b).length();
}

template<class T, size_t N>
Thelema::Vec<T, N> Thelema::Vec<T, N>::normalize() const {
    return this/length();
}

template<class T, size_t N>
T *Thelema::Vec<T, N>::operator[](size_t i) {
    return elements[i];
}


//MIT License

//This file is part of Thelema Engine

//Copyright (c) 2020 Vyacheslav

//Permission is hereby granted, free of charge, to any person obtaining a copy
//of this software and associated documentation files (the "Software"), to deal
//in the Software without restriction, including without limitation the rights
//to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//copies of the Software, and to permit persons to whom the Software is
//furnished to do so, subject to the following conditions:

//The above copyright notice and this permission notice shall be included in all
//copies or substantial portions of the Software.

//THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//SOFTWARE.


#include <thelema/core/scene_system.h>
#include <thelema/core/core.h>

Thelema::SceneSystem::SceneSystem()
        :
        _scenes(),
        _activeSceneIndex(0)
{
}

bool Thelema::SceneSystem::init()
{
    return true;
}

bool Thelema::SceneSystem::start()
{
    if (_scenes.empty())
    {
        Core::getInstance().getDebugSystem()->error(
                "SceneSystem::start()",
                "There is no scene that can be used");
        return false;
    }

    _scenes.at(_activeSceneIndex)->start();
    return true;
}

bool Thelema::SceneSystem::update()
{
    if (_scenes.empty())
    {
        Core::getInstance().getDebugSystem()->error(
                "SceneSystem::update()",
                "There is no scene that can be used");
        return false;
    }

    _scenes.at(_activeSceneIndex)->update();
    return true;
}

uint64 Thelema::SceneSystem::addScene(IScene* scene)
{
    std::shared_ptr<IScene> temp;
    temp.reset(scene);
    _scenes.push_back(std::move(temp));
    return _scenes.size() - 1;
}

bool Thelema::SceneSystem::setActiveScene(uint64 index)
{
    if (index >= _scenes.size())
    {
        Core::getInstance().getDebugSystem()->error(
                "IScene System",
                "There is no scene with index " + std::to_string(index));
        return false;
    }
    _activeSceneIndex = index;
    return true;
}

std::shared_ptr<Thelema::IScene> Thelema::SceneSystem::getActiveScene() const
{
    return _scenes.at(_activeSceneIndex);
}

uint64 Thelema::SceneSystem::getActiveSceneIndex() const
{
    return _activeSceneIndex;
}

Thelema::SceneSystem::~SceneSystem()
{

}

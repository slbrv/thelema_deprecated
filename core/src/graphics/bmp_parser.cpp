
#include <thelema/graphics/bmp_parser.h>
#include <fstream>


BMPImage::BMPImage(char *str) {
    std::ifstream fs;
    fs.open(str,std::ios_base::binary);
    if (!fs.is_open()) throw "Couldn't open the file";
    fs.read(reinterpret_cast<char *>(Header), sizeof(BMPHeader));
    fs.read(Data, *(uint32_t*)&Header[12] * *(uint32_t*)&Header[16]);
}

uint32_t BMPImage::GetWidth() {
    return *(uint32_t*)&Header[18];
}

uint32_t BMPImage::GetHeight() {
    return *(uint32_t*)&Header[22];
}

char *BMPImage::GetImageData() {
    return Data;
}

char *BMPImage::GetHeader() {
    return reinterpret_cast<char *>(Header);
}





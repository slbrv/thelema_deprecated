//MIT License

//This file is part of Thelema Engine

//Copyright (c) 2020 Vyacheslav

//Permission is hereby granted, free of charge, to any person obtaining a copy
//of this software and associated documentation files (the "Software"), to deal
//in the Software without restriction, including without limitation the rights
//to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//copies of the Software, and to permit persons to whom the Software is
//furnished to do so, subject to the following conditions:

//The above copyright notice and this permission notice shall be included in all
//copies or substantial portions of the Software.

//THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//SOFTWARE.


#ifndef THELEMA_ENGINE_SCENE_SYSTEM_H
#define THELEMA_ENGINE_SCENE_SYSTEM_H

#include <thelema/core/system.h>
#include <thelema/core/scene.h>
#include <thelema/core/types.h>

#include <vector>
#include <memory>

namespace Thelema
{
    class SceneSystem : public ISystem
    {
    public:
        SceneSystem();
        SceneSystem(const SceneSystem&) = delete;
        SceneSystem(const SceneSystem&&) = delete;

        SceneSystem operator=(const SceneSystem&) = delete;
        SceneSystem operator=(const SceneSystem&&) = delete;

        bool init() override;
        bool start() override;
        bool update() override;

        ///@param scene - New scene(will be moved)
        ///@return New scene index
        uint64 addScene(IScene* scene);
        ///After a new active scene is set the last scene will be disposed
        bool setActiveScene(uint64 index);

        std::shared_ptr<IScene> getActiveScene() const;
        uint64 getActiveSceneIndex() const;

        ~SceneSystem();
    private:
        std::vector<std::shared_ptr<IScene>> _scenes;
        uint64 _activeSceneIndex;
    };
}

#endif //THELEMA_ENGINE_SCENE_SYSTEM_H

//MIT License

//This file is part of Thelema Engine

//Copyright (c) 2020 Vyacheslav

//Permission is hereby granted, free of charge, to any person obtaining a copy
//of this software and associated documentation files (the "Software"), to deal
//in the Software without restriction, including without limitation the rights
//to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//copies of the Software, and to permit persons to whom the Software is
//furnished to do so, subject to the following conditions:

//The above copyright notice and this permission notice shall be included in all
//copies or substantial portions of the Software.

//THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//SOFTWARE.


#ifndef THELEMA_CORE_H
#define THELEMA_CORE_H

#include <string>

#include <thelema/core/types.h>
#include <thelema/graphics/window_system.h>
#include <thelema/core/scene_system.h>
#include <thelema/input/input_system.h>
#include <thelema/system/debug_system.h>
#include <thelema/data/file_system.h>
#include <thelema/graphics/graphics_system.h>
#include <thelema/audio/audio_system.h>

namespace Thelema
{
    class Core
    {
    public:

        static Core& getInstance();

        Core(const Core&) = delete;
        Core(const Core&&) = delete;

        Core& operator=(const Core&) = delete;

        bool init() noexcept;
        void start() noexcept;
        bool update() noexcept;

        std::shared_ptr<DebugSystem> getDebugSystem();
        std::shared_ptr<IWindowSystem> getWindowSystem();
        std::shared_ptr<IInputSystem> getInputSystem();
        std::shared_ptr<IFileSystem> getFileSystem();
        std::shared_ptr<IGraphicsSystem> getGraphicsSystem();
        std::shared_ptr<IAudioSystem> getAudioSystem();
        std::shared_ptr<SceneSystem> getSceneSystem();

        bool isInitialized() const;

        ~Core();
    private:
        Core();

        bool _initialized;

        std::shared_ptr<DebugSystem> _debugSystem;
        std::shared_ptr<IWindowSystem> _windowSystem;
        std::shared_ptr<IInputSystem> _inputSystem;
        std::shared_ptr<SceneSystem> _sceneSystem;
        std::shared_ptr<IFileSystem> _fileSystem;
        std::shared_ptr<IGraphicsSystem> _graphicsSystem;
        std::shared_ptr<IAudioSystem> _audioSystem;
    };
}

#endif //THELEMA_CORE_H

#ifndef THELEMA_VECTOR_H
#define THELEMA_VECTOR_H

#include <vector>
#include "istream"

namespace Thelema {

    template<class T = float, size_t N=0>
    class Vec {
    public:
        Vec();
        Vec (Vec& rhs);
        ~Vec();

        T* GetVec() const;
        size_t GetSize() const;

        T operator()(size_t i) const;
        T* operator[](size_t i);

        Vec operator=(Vec rhs);

        Vec operator+(Vec rhs) const; //vec a + vec b =(a[0]+b[0],a[1]+b[1],..)
        Vec operator-(Vec rhs) const;
        Vec operator*(Vec rhs) const;
        Vec operator/(Vec rhs) const;

        Vec operator+(float rhs) const; //vec a + float b =(a[0]+b,a[1]+b,..)
        Vec operator-(float rhs) const;
        Vec operator*(float rhs) const;
        Vec operator/(float rhs) const;

        T *operator()(size_t i);

        float dot(Vec a, Vec b) const;      // a[0]*b[0]+a[1]*b[1]+...
        float length() const;          // sqrt(dot(a,a))
        float distance(Vec b) const; // length (a-b)
        Vec normalize() const;         // a/length(a)

    private:
        size_t vector_size;
        std::vector<std::vector<T>> elements;

    };
}

#endif //THELEMA_VECTOR_H

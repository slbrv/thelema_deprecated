
#ifndef THELEMA_GL_VECTOR_H
#define THELEMA_GL_VECTOR_H

#include "vec.h"

namespace Thelema {
    typedef Vec<int, 1> ivec1;
    typedef Vec<int, 2> ivec2;
    typedef Vec<int, 3> ivec3;
    typedef Vec<int, 4> ivec4;

    typedef Vec<float, 1> vec1;
    typedef Vec<float, 2> vec2;
    typedef Vec<float, 3> vec3;
    typedef Vec<float, 4> vec4;

    typedef Vec<bool, 1> bvec1;
    typedef Vec<bool, 2> bvec2;
    typedef Vec<bool, 3> bvec3;
    typedef Vec<bool, 4> bvec4;

    typedef Vec<double, 1> dvec1;
    typedef Vec<double, 2> dvec2;
    typedef Vec<double, 3> dvec3;
    typedef Vec<double, 4> dvec4;

    typedef Vec<unsigned int, 1> uvec1;
    typedef Vec<unsigned int, 2> uvec2;
    typedef Vec<unsigned int, 3> uvec3;
    typedef Vec<unsigned int, 4> uvec4;
}

#endif //THELEMA_GL_VECTOR_H

#ifndef THELEMA_ENGINE_MATRIX_H
#define THELEMA_ENGINE_MATRIX_H

#include <vector>
#include <istream>
#include <memory>
#include <cstring>
#include <thelema/exceptions/math_exceptions.h>
#include <cmath>

namespace Thelema {
    template<size_t R, size_t C, class T = double>
    class Matrix {
    public:
        Matrix();

        Matrix(const Matrix &rhs);

        Matrix(Matrix &&rhs) noexcept ;

        Matrix &operator=(const Matrix &rhs);

        Matrix &operator=(Matrix &&rhs) noexcept ;


        size_t getRows() const;

        size_t getCols() const;

        T operator()(size_t i, size_t j) const;

        T &operator()(size_t i, size_t j);

        bool operator==(const Matrix &rhs) const;

        bool operator!=(const Matrix &rhs) const;

        Matrix operator+(const Matrix &rhs) const;

        Matrix operator-(const Matrix &rhs) const;

        template<size_t rhsC>
        Matrix<R, rhsC, T> operator*(const Matrix<C, rhsC, T> &rhs) const;

        Matrix operator*(T val) const;

        friend
        Matrix operator*(T val, const Matrix &matrix);

        Matrix transp() const;

        T det() const;

        Matrix adj() const;

        Matrix inv() const;

        std::shared_ptr<T> getShaderPtr();

    private:

        Matrix(size_t row, size_t col);

        T getSupplement(size_t row, size_t col) const;

        Matrix getMinorMatrix(size_t row, size_t col) const;

        bool isAlmostEqual(T x, T y) const;

        T roundToFifthDigit(T num) const;

        size_t _rows;
        size_t _cols;
        std::shared_ptr<T> _elements;
    };

    template<size_t R, size_t C, class T>
    Matrix<R, C, T>::Matrix() : Matrix(R, C) {}

    template<size_t R, size_t C, class T>
    std::shared_ptr<T> Matrix<R, C, T>::getShaderPtr() {
        return _elements;
    }

    template<size_t R, size_t C, class T>
    size_t Matrix<R, C, T>::getRows() const {
        return _rows;
    }

    template<size_t R, size_t C, class T>
    size_t Matrix<R, C, T>::getCols() const {
        return _cols;
    }

    template<size_t R, size_t C, class T>
    Matrix<R, C, T>::Matrix(const Matrix &rhs) : _rows(rhs.getRows()), _cols(rhs.getCols()),
                                                 _elements(new T[_rows * _cols], std::default_delete<T[]>()) {
        for (int i = 0; i < _rows * _cols; ++i) {
            _elements.get()[i] = rhs._elements.get()[i];
        }
    }

    template<size_t R, size_t C, class T>
    Matrix<R, C, T>::Matrix(Matrix &&rhs) noexcept : _rows(rhs.getRows()), _cols(rhs.getCols()), // TODO: alarm
                                            _elements(std::move(rhs._elements)) {
    }

    template<size_t R, size_t C, class T>
    Matrix<R, C, T> &Matrix<R, C, T>::operator=(const Matrix &rhs) {
        _rows = rhs.getRows();
        _cols = rhs.getCols();
        _elements = std::shared_ptr<T>(new T[_rows * _cols], std::default_delete<T[]>());
        for (int i = 0; i < _rows * _cols; ++i) {
            _elements.get()[i] = rhs._elements.get()[i];
        }
        return *this;
    }

    template<size_t R, size_t C, class T>
    Matrix<R, C, T> &Matrix<R, C, T>::operator=(Matrix &&rhs) noexcept { // TODO: alarm
        _rows = rhs.getRows();
        _cols = rhs.getCols();
        _elements = std::move(rhs._elements);
        return *this;
    }

    template<size_t R, size_t C, class T>
    T Matrix<R, C, T>::operator()(size_t i, size_t j) const {
        if (i > _rows - 1 || j > _cols - 1) {
            throw OutOfRange(i, j);
        }
        return _elements[_cols * i + j];
    }

    template<size_t R, size_t C, class T>
    T &Matrix<R, C, T>::operator()(size_t i, size_t j) {
        if (i > _rows - 1 || j > _cols - 1) {
            throw OutOfRange(i, j);
        }
        return _elements[_cols * i + j];
    }

    template<size_t R, size_t C, class T>
    bool Matrix<R, C, T>::operator==(const Matrix &rhs) const {
        if (_elements == &rhs._elements) {
            return true;
        }

        if ((_cols != rhs._cols) || (_rows != rhs._rows)) {
            return false;
        }

        for (size_t row = 0; row < _rows; row++) {
            for (size_t col = 0; col < _cols; col++) {
                if (!isAlmostEqual(operator()(row, col), rhs(row, col))) {
                    return false;
                }
            }
        }
        return true;
    }

    template<size_t R, size_t C, class T>
    bool Matrix<R, C, T>::operator!=(const Matrix &rhs) const {
        return !operator==(rhs);
    }

    template<size_t R, size_t C, class T>
    Matrix<R, C, T> Matrix<R, C, T>::operator+(const Matrix &rhs) const {
        if (_rows != rhs.getRows() || _cols != rhs.getCols()) {
            throw DimensionMismatch<Matrix>(*this, rhs);
        }

        Matrix<R, C, T> sum_matrix;
        for (size_t i = 0; i < sum_matrix._rows; i++) {
            for (size_t j = 0; j < sum_matrix._cols; j++) {
                sum_matrix(i, j) = operator()(i, j) + rhs(i, j);
            }
        }

        return sum_matrix;
    }

    template<size_t R, size_t C, class T>
    Matrix<R, C, T> Matrix<R, C, T>::operator-(const Matrix &rhs) const {
        if (_rows != rhs.getRows() || _cols != rhs.getCols()) {
            throw DimensionMismatch<Matrix>(*this, rhs);
        }

        Matrix<R, C, T> sub_matrix;
        for (size_t i = 0; i < sub_matrix._rows; i++) {
            for (size_t j = 0; j < sub_matrix._cols; j++) {
                sub_matrix(i, j) = operator()(i, j) - rhs(i, j);
            }
        }

        return sub_matrix;
    }

    template<size_t R, size_t C, class T>
    template<size_t rhsC>
    Matrix<R, rhsC, T> Matrix<R, C, T>::operator*(const Matrix<C, rhsC, T> &rhs) const {
        if (_cols != rhs.getRows()) {
            throw DimensionMismatch<Matrix>(*this, rhs);
        }

        size_t index_sum = rhs.getRows();
        Matrix<R, rhsC, T> result_matrix;

        for (size_t i = 0; i < _rows; i++) {
            for (size_t j = 0; j < rhs.getCols(); j++) {
                T element = 0;
                for (size_t k = 0; k < index_sum; k++) {
                    element += operator()(i, k) * rhs(k, j);
                }
                result_matrix(i, j) = element;
            }
        }
        return result_matrix;
    }

    template<size_t R, size_t C, class T>
    Matrix<R, C, T> Matrix<R, C, T>::operator*(T val) const {
        Matrix multiplied_matrix;
        for (size_t i = 0; i < multiplied_matrix._rows; ++i) {
            for (size_t j = 0; j < multiplied_matrix._cols; ++j) {
                multiplied_matrix(i, j) = operator()(i, j) * val;
            }
        }
        return multiplied_matrix;
    }

    template<size_t R, size_t C, class T>
    Matrix<R, C, T> operator*(T val, const Matrix<R, C, T> &matrix) {
        return matrix * val;
    }

    template<size_t R, size_t C, class T>
    Matrix<R, C, T> Matrix<R, C, T>::transp() const {
        Matrix transposed_matrix;
        for (size_t i = 0; i < transposed_matrix.getRows(); i++) {
            for (size_t j = 0; j < transposed_matrix.getCols(); ++j) {
                transposed_matrix(i, j) = operator()(j, i);
            }
        }
        return transposed_matrix;
    }

    template<size_t R, size_t C, class T>
    T Matrix<R, C, T>::det() const {
        if (_cols != _rows) {
            throw NonSquare<Matrix>(*this);
        }

        // Base case:
        if (C == 2) {
            return operator()(0, 0) * operator()(1, 1) -
                   operator()(1, 0) * operator()(0, 1);
        } else if (C == 1) {
            return operator()(0, 0);
        }

        T result = 0;
        for (size_t i = 0; i < _cols; i++) {  // Expand on the first line
            result += operator()(0, i) * getSupplement(0, i);
        }
        return result;
    }

    template<size_t R, size_t C, class T>
    T Matrix<R, C, T>::getSupplement(size_t row, size_t col) const {
        return pow(-1, static_cast<double>(row + col)) * getMinorMatrix(row, col).det();
    }

    template<size_t R, size_t C, class T>
    Matrix<R, C, T> Matrix<R, C, T>::getMinorMatrix(size_t row, size_t col) const {
        if (_rows == 1 && _cols == 1) {
            Matrix minor_matrix(_rows, _cols);
            minor_matrix(0, 0) = 1;
            return minor_matrix;
        }

        Matrix minor_matrix(_rows - 1, _cols - 1);
        size_t minor_matrix_i = 0;
        size_t minor_matrix_j = 0;

        for (size_t i = 0; i < _rows; ++i) {
            if (i == row) continue;
            for (size_t j = 0; j < _cols; ++j) {
                if (j == col) continue;
                minor_matrix(minor_matrix_i, minor_matrix_j) = operator()(i, j);
                minor_matrix_j++;
            }
            minor_matrix_j = 0;
            minor_matrix_i++;
        }
        return minor_matrix;
    }

    template<size_t R, size_t C, class T>
    Matrix<R, C, T>::Matrix(size_t row, size_t col) :_rows(row), _cols(col), _elements(new T[_rows * _cols], std::default_delete<T[]>()){}

    template<size_t R, size_t C, class T>
    Matrix<R, C, T> Matrix<R, C, T>::adj() const {
        if (_cols != _rows) {
            throw NonSquare<Matrix>(*this);
        }

        Matrix adj_matrix(_cols, _rows);
        Matrix transp_matrix = transp();

        for (size_t i = 0; i < transp_matrix.getRows(); ++i) {
            for (size_t j = 0; j < transp_matrix.getCols(); ++j) {
                adj_matrix(i, j) = transp_matrix.getSupplement(i, j);
            }
        }
        return adj_matrix;
    }

    template<size_t R, size_t C, class T>
    Matrix<R, C, T> Matrix<R, C, T>::inv() const {
        T determ = det();
        if (determ == 0) {
            throw SingularMatrix();
        }
        Matrix inv_matrix = adj() * (1 / determ);
        return inv_matrix;
    }

    template<size_t R, size_t C, class T>
    bool Matrix<R, C, T>::isAlmostEqual(T x, T y) const {
        x = roundToFifthDigit(x);
        y = roundToFifthDigit(y);
        return x == y;
    }

    template<size_t R, size_t C, class T>
    T Matrix<R, C, T>::roundToFifthDigit(T num) const {
        return std::trunc(num * 1.0e+05) * 1.0e-05;
    }

}  // namespace Thelema

#endif //THELEMA_ENGINE_MATRIX_H

#ifndef THELEMA_ENGINE_GL_MATRIX_H
#define THELEMA_ENGINE_GL_MATRIX_H

#include "matrix.h"

namespace Thelema {
    /// a 2×2 single-precision floating-point matrix
    using mat2 = Matrix<2, 2, float>;
    /// a 3×3 single-precision floating-point matrix
    using mat3 = Matrix<3, 3, float>;
    /// a 4×4 single-precision floating-point matrix
    using mat4 = Matrix<4, 4, float>;
    /// same as a mat2
    using mat2x2 = Matrix<2, 2, float>;
    /// a single-precision floating-point matrix with 2 columns and 3 rows
    using mat2x3 = Matrix<2, 3, float>;
    /// a single-precision floating-point matrix with 2 columns and 4 rows
    using mat2x4 = Matrix<2, 4, float>;
    /// a single-precision floating-point matrix with 3 columns and 2 rows
    using mat3x2 = Matrix<3, 2, float>;
    /// same as a mat3
    using mat3x3 = Matrix<3, 3, float>;
    /// a single-precision floating-point matrix with 3 columns and 4 rows
    using mat3x4 = Matrix<3, 4, float>;
    /// a single-precision floating-point matrix with 4 columns and 2 rows
    using mat4x2 = Matrix<4, 2, float>;
    /// a single-precision floating-point matrix with 4 columns and 3 rows
    using mat4x3 = Matrix<4, 3, float>;
    /// same as a mat4
    using mat4x4 = Matrix<4, 4, double>;
    /// a 2×2 double-precision floating-point matrix
    using dmat2 = Matrix<2, 2, double>;
    /// a 3×3 double-precision floating-point matrix
    using dmat3 = Matrix<3, 3, double>;
    /// a 4×4 double-precision floating-point matrix
    using dmat4 = Matrix<4, 4, double>;
    ///  same as a dmat2
    using dmat2x2 = Matrix<2, 2, double>;
    /// a double-precision floating-point matrix with 2 columns and 3 rows
    using dmat2x3 = Matrix<2, 3, double>;
    /// a double-precision floating-point matrix with 2 columns and 4 rows
    using dmat2x4 = Matrix<2, 4, double>;
    /// a double-precision floating-point matrix with 3 columns and 2 rows
    using dmat3x2 = Matrix<3, 2, double>;
    ///  same as a dmat3
    using dmat3x3 = Matrix<3, 3, double>;
    /// a double-precision floating-point matrix with 3 columns and 4 rows
    using dmat3x4 = Matrix<3, 4, double>;
    /// a double-precision floating-point matrix with 4 columns and 2 rows
    using dmat4x2 = Matrix<4, 2, double>;
    /// a double-precision floating-point matrix with 4 columns and 3 rows
    using dmat4x3 = Matrix<4, 3, double>;
    /// same as a dmat4
    using dmat4x4 = Matrix<4, 4, double>;

}

#endif //THELEMA_ENGINE_GL_MATRIX_H

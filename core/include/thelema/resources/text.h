#ifndef THELEMA_ENGINE_TEXT_H
#define THELEMA_ENGINE_TEXT_H

#include <string>
#include <thelema/resources/iresource.h>

namespace Thelema {
    class Text : IResource {
    public:

        bool loadFromFile(fs::path &path) override;

        const std::vector<std::string> &getResourcesExtensions() override;

        const char *getCStr() const;

        std::string &getString();

    private:
        std::string _text;
    };
}


#endif //THELEMA_ENGINE_TEXT_H

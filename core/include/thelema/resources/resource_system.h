#ifndef THELEMA_ENGINE_RESOURCESYSTEM_H
#define THELEMA_ENGINE_RESOURCESYSTEM_H

// Include
#include <experimental/filesystem>
#include <map>
#include "thelema/exceptions/systems_exceptions.h"
#include "thelema/core/system.h"
#include "thelema/resources/iresource.h"

namespace fs = std::experimental::filesystem;

namespace  Thelema {
    class Texture;
    class AbstractShader;
    class Text;
    class Sound;
}

namespace Thelema {

    class ResourceSystemWrapper : public ISystem {
    protected:
        std::map<std::string, std::unique_ptr<IResource>> _resourceMap;
    };


    class ResourceSystem : public ResourceSystemWrapper {

    public:
        /// load all know types resources from MEDIA_ROOT
        bool init() override;

        bool start() override;

        bool update() override;

        template<class Resource>
        Resource &get(const std::string &resourceName);

    private:
        template<class Resource>
        bool loadResource(fs::path path);
    };


    template<class Resource>
    Resource &ResourceSystem::get(const std::string &resourceName) {
        auto it = _resourceMap.find(resourceName);
        if (it == _resourceMap.end()) {
            throw ResourceGetException(resourceName);
        }
        return dynamic_cast<Resource&>(*(it->second.get()));
    }

    template<class Resource>
    bool ResourceSystem::loadResource(fs::path path) {
        bool retCode = true;
        for (auto &file : fs::directory_iterator(path)) {
            // Check that file extension valid:
            auto it = Resource::getResourcesExtensions().find(path.extension().string());
            if (it == Resource::getResourcesExtensions().end()) {
                continue;
            }
            // Load resource:
            auto resource = std::make_unique<Resource>();
            if(resource.loadFromFile(path)) {
                _resourceMap.insert(std::make_pair(path.filename().string(), std::move(resource)));
            } else {
                retCode = false;
                break;
            }
        }
        return retCode;
    }

}  // Thelema

#endif //THELEMA_ENGINE_RESOURCESYSTEM_H

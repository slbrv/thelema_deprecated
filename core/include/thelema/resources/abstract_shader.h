#ifndef THELEMA_ENGINE_ABSTRACT_SHADER_H
#define THELEMA_ENGINE_ABSTRACT_SHADER_H
// Include
#include <string>
#include <thelema/resources/iresource.h>
#include <memory>
#include "text.h"

namespace Thelema {
    class AbstractShader : IResource {
    public:
        enum Type {
            Vertex,
            Geometry,
            Fragment,
            None
        };
    public:
        AbstractShader();

        bool loadFromFile(fs::path &path) override;

        const std::vector<std::string> &getResourcesExtensions() override;

        virtual bool compile() = 0;

        virtual bool use() = 0;

    protected:
        static std::unique_ptr<char[]> getShaderText(const fs::path &path);

        unsigned int _shader;
        Type _type;
        std::unique_ptr<char[]> _shaderText;

    };
} //namespace Thelema

#endif //THELEMA_ENGINE_ABSTRACT_SHADER_H

#ifndef THELEMA_ENGINE_SHADER_PROGRAM_H
#define THELEMA_ENGINE_SHADER_PROGRAM_H

#include <thelema/resources/iresource.h>
#include <thelema/resources/abstract_shader.h>
#include <vector>

namespace Thelema {
    class AbstractShaderProgram : IResource {
    public:
        AbstractShaderProgram();

        virtual AbstractShaderProgram &attachShader(AbstractShader shader) = 0;

        virtual void useProgram() = 0;

        const std::vector<std::string> &getResourcesExtensions() override;

    protected:
        virtual unsigned int createProgram() const = 0;

        unsigned int _shaderProgram;
    };
}  // Thelema

#endif //THELEMA_ENGINE_SHADER_PROGRAM_H

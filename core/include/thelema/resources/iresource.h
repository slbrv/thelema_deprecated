#ifndef THELEMA_ENGINE_IRESOURCE_H
#define THELEMA_ENGINE_IRESOURCE_H
#include <vector>
#include <string>
#include <experimental/filesystem>

namespace fs = std::experimental::filesystem;

namespace Thelema {
    class IResource {
    public:
        virtual bool loadFromFile(fs::path &path) = 0;
        virtual const std::vector<std::string> &getResourcesExtensions() = 0;
    };
} // Thelema

#endif //THELEMA_ENGINE_IRESOURCE_H

#ifndef THELEMA_ENGINE_RESOURCESTYPES_H
#define THELEMA_ENGINE_RESOURCESTYPES_H

namespace Thelema {
    enum Textures {
        TexturesCnt
    };

    enum Sounds {
        SoundsCnt
    };

    enum Fonts {
        FontsCnt
    };

    enum Texts {
        TextsCnt
    };

}  // namespace Thelema

#endif //THELEMA_ENGINE_RESOURCESTYPES_H

#ifndef THELEMA_ENGINE_RESOURCE_HOLDER_H
#define THELEMA_ENGINE_RESOURCE_HOLDER_H

#include <memory>
#include <map>
#include "resources_types.h"

namespace Thelema {

    template<class Resource, class ResourceType>
    class ResourceHolder {
    public:

        ResourceHolder(const ResourceHolder &resourceHolder) = delete;
        ResourceHolder(const ResourceHolder &&resourceHolder) = delete;
        ResourceHolder &operator=(const ResourceHolder &resourceHolder) = delete;
        ResourceHolder &operator=(const ResourceHolder &&resourceHolder) = delete;

        void add(ResourceType type, std::string path) {
            auto resource = std::make_unique<Resource>();
            if(resource.loadFromFile(path)) {
                _map.insert(std::make_pair(type, std::move(resource)));
            } else {
                throw std::runtime_error("Can not load resource");
            }
        };

        Resource &get(ResourceType type) {
            auto it = _map.find(type);
            if (it == _map.end()) {
                throw std::runtime_error("Resource not found!");
            }
            return &it->second;
        };

        ResourceHolder<Resource, ResourceType> &getInstance() {
            if (!_resourceHolder) {
                _resourceHolder = new ResourceHolder;
            }
            return *_resourceHolder;
        };

    private:
        ResourceHolder() = default;
        ~ResourceHolder() = default;

        static /*inline*/ ResourceHolder<Resource, ResourceType> *_resourceHolder;
        std::map<ResourceType, std::unique_ptr<Resource>> _map;
    };

    template<class Resource, class ResourceType>
    ResourceHolder<Resource, ResourceType> *ResourceHolder<Resource, ResourceType>::_resourceHolder = nullptr;

    class Texture;
    class Sound;
    class Text;
    using TextHolder = ResourceHolder<Text, Texts>;
    using SoundsHolder = ResourceHolder<Sound, Sounds>;
    using TextureHolder = ResourceHolder<Texture, Textures>;

}  // namespace Thelema
#endif //THELEMA_ENGINE_RESOURCE_HOLDER_H

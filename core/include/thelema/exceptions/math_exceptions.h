#ifndef THELEMA_ENGINE_MATH_EXCEPTIONS_H
#define THELEMA_ENGINE_MATH_EXCEPTIONS_H

#include <string>
#include "base_exception.h"

namespace Thelema {
    class OutOfRange : public EngineException {
    public:
        OutOfRange(size_t i, size_t j);
    };

    class SingularMatrix : public EngineException {
    public:
        SingularMatrix();
    };

    template<class M>
    class NonSquare : public EngineException {
    public:
        explicit NonSquare(const M matrix) : EngineException(
            "M[" + std::to_string(matrix.getRows()) + ", " + std::to_string(matrix.getCols()) +
            "] isn't square matrix") {
        }
    };

    template<class M>
    class DimensionMismatch : EngineException {
        DimensionMismatch(const M &matrix1, const M &matrix2) : EngineException(
            "M1[" + std::to_string(matrix1.getRows()) + ", " + std::to_string(matrix1.getCols()) + "] and M2[" +
            std::to_string(matrix2.getRows()) + ", " + std::to_string(matrix2.getCols()) + "] are not compatible") {}
    };

}  // Thelema

#endif //THELEMA_ENGINE_MATH_EXCEPTIONS_H

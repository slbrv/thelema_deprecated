#ifndef THELEMA_ENGINE_SYSTEMS_EXCEPTIONS_H
#define THELEMA_ENGINE_SYSTEMS_EXCEPTIONS_H
#include <thelema/exceptions/base_exception.h>

namespace Thelema {
    class ResourceLoadException : EngineException {
    public:
        explicit ResourceLoadException(std::string message);
    };

    class ResourceGetException : EngineException {
    public:
        explicit ResourceGetException(std::string message);
    };

}  // Thelema


#endif //THELEMA_ENGINE_SYSTEMS_EXCEPTIONS_H

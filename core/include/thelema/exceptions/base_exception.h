#ifndef THELEMA_ENGINE_BASE_EXEPTION_H
#define THELEMA_ENGINE_BASE_EXEPTION_H

#include <exception>
#include <string>
#include <utility>

namespace Thelema {
    class EngineException : public std::exception {
    public:
        explicit EngineException(std::string message);
        const char *what() const noexcept override;

    private:
        std::string _message;
    };

}  // namespace Thelema

#endif //THELEMA_ENGINE_BASE_EXEPTION_H

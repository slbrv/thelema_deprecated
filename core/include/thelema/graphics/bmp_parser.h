#ifndef THELEMA_ENGINE_BMP_PARSER_H
#define THELEMA_ENGINE_BMP_PARSER_H

#include <cstdio>
#include <cstdint>

// definition of BMP file structure

typedef struct BMPHeader{
    unsigned char Sign[2];
    uint32_t filesz;
    uint16_t creator1;
    uint16_t creator2;
    uint32_t bpm_offset;
} BMPHeader;

typedef struct DIBHeader {
    uint32_t header_sz;
    int32_t width;
    int32_t height;
    uint16_t nplanes;
    uint16_t bitspp;
    uint32_t compress_type;
    uint32_t bmp_bytesz;
    int32_t hres;
    int32_t vres;
    uint32_t ncolors;
    uint32_t nimpcolors;
    } DIBHeader;

typedef struct ColorTable {
    unsigned char r,g,b,a;
} ColorTable;

class BMPImage{ //this works with 32-bit encoded bmp with no compression
    private:
    unsigned char Header[sizeof(BMPHeader)];
    char *Data;
    public:

    explicit BMPImage(char *path);
    ~BMPImage();

    uint32_t GetWidth();
    uint32_t GetHeight();
    char * GetImageData();
    char* GetHeader();
};
#endif //THELEMA_ENGINE_BMP_PARSER_H
// glTextImage2D( target,level,internalFormat,width,height,border,format,dataType,data)
// to use glTextImage2D in OpenGl with this parser you need to write this code
// glTextImage2D( GL_TEXTURE_2D,0,GL_RGBA,BMPImage.GetWidth(),BMPImage.GetHeight(),0,GL_BGRA_INTEGER,,BMPImage.GetImageData())
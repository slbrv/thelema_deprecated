//MIT License

//This file is part of Thelema Engine

//Copyright (c) 2020 Vyacheslav

//Permission is hereby granted, free of charge, to any person obtaining a copy
//of this software and associated documentation files (the "Software"), to deal
//in the Software without restriction, including without limitation the rights
//to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//copies of the Software, and to permit persons to whom the Software is
//furnished to do so, subject to the following conditions:

//The above copyright notice and this permission notice shall be included in all
//copies or substantial portions of the Software.

//THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//SOFTWARE.


#ifndef THELEMA_ENGINE_WINDOW_SYSTEM_H
#define THELEMA_ENGINE_WINDOW_SYSTEM_H

#include <vector>
#include <thelema/graphics/window.h>
#include <thelema/core/types.h>
#include <thelema/core/system.h>

namespace Thelema
{
    class IWindowSystem : public ISystem
    {
    public:
        IWindowSystem() = default;
        IWindowSystem(const IWindowSystem&) = delete;
        IWindowSystem(const IWindowSystem&&) = delete;

        virtual void addWindow(std::shared_ptr<Window>& window) = 0;

        virtual void setActiveWindow(uint64 index) = 0;

        virtual std::shared_ptr<Window> getActiveWindow() = 0;

        ~IWindowSystem() override = default;
    private:
    };
}

#endif //THELEMA_ENGINE_WINDOW_SYSTEM_H

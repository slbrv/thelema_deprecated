//MIT License

//This file is part of Thelema Engine

//Copyright (c) 2020 Vyacheslav

//Permission is hereby granted, free of charge, to any person obtaining a copy
//of this software and associated documentation files (the "Software"), to deal
//in the Software without restriction, including without limitation the rights
//to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//copies of the Software, and to permit persons to whom the Software is
//furnished to do so, subject to the following conditions:

//The above copyright notice and this permission notice shall be included in all
//copies or substantial portions of the Software.

//THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//SOFTWARE.


#ifndef THELEMA_ENGINE_WINDOW_H
#define THELEMA_ENGINE_WINDOW_H

#include <string>
#include <memory>
#include <thelema/core/types.h>

namespace Thelema
{
    class Window
    {
    public:
        enum State
        {
            CLOSED,
            INACTIVE,
            ACTIVE
        };

        Window();
        Window(const Window&) = default;
        Window(std::string&& title, const uint64& width, const uint64& height);

        bool init();
        void start();
        bool update();

        void activate();
        void deactivate();

        Window& operator=(const Window&) = default;

        void* getWindow();
        State getState();

        ~Window();
    private:
        bool _initialized;
        State _state;

        std::string _title;
        uint64 _width, _height;
        void* _window;
    };
};

#endif //THELEMA_ENGINE_WINDOW_H
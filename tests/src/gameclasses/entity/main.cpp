
#include "gtest/gtest.h"
#include "entity.h"
#include "experimental/filesystem"
std::experimental::filesystem::path Data;
int main(int argc, char** argv) {
    testing::InitGoogleTest(&argc, argv);
    if (argc < 2) {
        return -1;
    }
    Data = argv[1];
    std::cout << "Path to data dir: " << argv[1] << std::endl;
    return RUN_ALL_TESTS();
}
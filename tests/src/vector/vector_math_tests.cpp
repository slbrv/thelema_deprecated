#include "gtest/gtest.h"
#include "vec.h"
#include "vector_utills.h"
#include "fstream"

TEST (VectorMath,correct_multiplication){

    Thelema::Vec<double,4> vector1=getVectorfromFile();
    Thelema::Vec<double,4> vector2=getVectorfromFile();
    Thelema::Vec<double,4> resvector=vector1*vector2;
    for (size_t i=0; i<resvector.GetSize();++i){
        ASSERT_DOUBLE_EQ(*resvector[i],*vector1[i] * *vector2[i]);
    }
}

TEST (VectorMath,correct_float_multiplication){
    Thelema::Vec<double,4>  vector1=getVectorfromFile();
    const float float1=GetRandom();
    Thelema::Vec<double,4> resvector1=vector1*float1;
    for (size_t i=0; i<resvector1.GetSize();++i){
        ASSERT_DOUBLE_EQ(*resvector1[i],*vector1[i] * float1);
    }
}
TEST (VectorMath,correct_sum){
    Thelema::Vec<double,4> vector1=getVectorfromFile();
    Thelema::Vec<double,4> vector2=getVectorfromFile();
    Thelema::Vec<double,4> resvector=vector1+vector2;
    for (size_t i=0; i<resvector.GetSize();++i){
        ASSERT_DOUBLE_EQ(*resvector[i],*vector1[i]+ *vector2[i]);
    }
}
TEST (VectorMath,correct_float_sum){
    Thelema::Vec<double,4> vector1=GetVector<double>();
    const float float1=GetRandom();
    Thelema::Vec<double,4> resvector1=vector1+float1;
    for (size_t i=0; i<resvector1.GetSize();++i){
        ASSERT_DOUBLE_EQ(*resvector1[i],*vector1[i] + float1);
    }
}
TEST (VectorMath,correct_sub){

    Thelema::Vec<double,4> vector1=getVectorfromFile();
    Thelema::Vec<double,4> vector2=getVectorfromFile();
    Thelema::Vec<double,4> resvector=vector1-vector2;
    for (size_t i=0; i<resvector.GetSize();++i){
        ASSERT_DOUBLE_EQ(*resvector[i],*vector1[i] - *vector2[i]);
    }
}

TEST (VectorMath,correct_float_sub){
    Thelema::Vec<double,4>  vector1=getVectorfromFile();
    const float float1=GetRandom();
    Thelema::Vec<double,4> resvector1=vector1-float1;
    for (size_t i=0; i<resvector1.GetSize();++i){
        ASSERT_DOUBLE_EQ(*resvector1[i],*vector1[i] - float1);
    }
}
TEST (VectorMath,correct_vector_dot){
    Thelema::Vec<double,4>  vector1=getVectorfromFile();
    Thelema::Vec<double,4>  vector2=getVectorfromFile();
    Thelema::Vec<double,4> resvector;
    float result=resvector.dot(vector1,vector2);
    float check=0;
    for (size_t i=0;i<resvector.GetSize();++i){
        check+= *vector1[i] * *vector2[i];
    }
    ASSERT_DOUBLE_EQ(result,check);
}
TEST (VectorMath,correct_vector_length){
    Thelema::Vec<double,4>  vector1=getVectorfromFile();
    float result=vector1.length();
    double check=sqrt(vector1.dot(vector1,vector1));
    ASSERT_DOUBLE_EQ(result,check);
}

TEST (VectorMath,correct_vector_distance){
    Thelema::Vec<double,4>  vector1=getVectorfromFile();
    Thelema::Vec<double,4>  vector2=getVectorfromFile();
    float result=vector1.distance(vector2);
    float check= (vector2-vector1).length();
    ASSERT_DOUBLE_EQ(result,check);
}
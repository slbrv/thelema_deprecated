#include "utills.h"
#include "vec.h"

Thelema::Vec<double,4> getVectorfromFile(std::ifstream &in){
    Thelema::Vec<double,4> vector1;
    for (int i=0;i<4;i++) {
        in>>vector1(i);
    }
    return vector1;
}
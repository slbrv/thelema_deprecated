#include "gtest/gtest.h"
#include "vec.h"

#include "vector_utills.h"

TEST(BaseVectorTest,ConstructorAndBaseFunctions){
    Thelema::Vec<float,4> vector1;
    ASSERT_EQ(vector1.GetSize(),4);
    ASSERT_NO_THROW(vector1[1]);
}

TEST(BaseVectorTest, ConstructorCopy) {
    auto vector1 = GetVector<float>();
    auto vector2(vector1);
    ASSERT_FALSE(vector1.GetVec() == vector2.GetVec());
    for (size_t i=0; i<vector1.GetSize();++i)
    ASSERT_EQ(*vector1[i], *vector2[i]);
}

TEST(BaseVectorTest, CopyAssignment) {
    auto vector1 = GetVector<float>();
    Thelema::Vec<float,4> vector2;
    vector2 = vector1;
    ASSERT_FALSE(vector1.GetVec() == vector2.GetVec());
    for (size_t i=0; i<vector1.GetSize();++i)
        ASSERT_EQ(*vector1[i], *vector2[i]);
}

TEST(BaseVectorTest, MoveAssignment) {
    auto vector1 = GetVector<float>();
    const auto ptr = vector1.GetVec();
    Thelema::Vec<float,4> vector2;
    vector2 = vector1;
    ASSERT_EQ(vector2.GetVec(), ptr);
    ASSERT_EQ(vector1.GetVec(), nullptr);
}
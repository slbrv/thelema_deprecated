#include <gtest/gtest.h>
#include "matrix_utils.h"

fs::path matrixDataPath;

int main(int argc, char** argv) {
    testing::InitGoogleTest(&argc, argv);
    if (argc < 2) {
        return -1;
    }
    matrixDataPath = argv[1];
    std::cout << "Path to data dir: " << argv[1] << std::endl;
    return RUN_ALL_TESTS();
}

#include <memory>

#include "audio_utils.h"

void BaseTest::SetUp() {
    Test::SetUp();
    _soundHolder = std::make_unique<Thelema::SoundsHolder>();
    _soundHolder->add((resourcePath / fs::path("sounds") / fs::path("sample.waw")).c_str(),
                     Thelema::Sounds::SoundsCnt);
    _audio = std::make_unique<Thelema::Audio>(*_soundHolder);
}

void BaseTest::TearDown() {
    Test::TearDown();
}

#include "audio_utils.h"

TEST_F(BaseTest, playSound) {
    ASSERT_TRUE(_audio->play(Thelema::Sounds::SoundsCnt));
}

TEST_F(BaseTest, stop) {
    ASSERT_TRUE(_audio->stop());
    ASSERT_FALSE(_audio->stop());
}
TEST_F(BaseTest, resume) {
    _audio->stop();
    ASSERT_TRUE(_audio->resume());
    ASSERT_FALSE(_audio->resume());
}

TEST_F(BaseTest, stopSound) {
    _audio->play(Thelema::Sounds::SoundsCnt);
    ASSERT_TRUE(_audio->stop(Thelema::Sounds::SoundsCnt));
    ASSERT_FALSE(_audio->stop(Thelema::Sounds::SoundsCnt));
}

TEST_F(BaseTest, resumeSound) {
    _audio->play(Thelema::Sounds::SoundsCnt);
    _audio->stop(Thelema::Sounds::SoundsCnt);
    ASSERT_TRUE(_audio->resume(Thelema::Sounds::SoundsCnt));
    ASSERT_FALSE(_audio->resume(Thelema::Sounds::SoundsCnt));
}

#include <gtest/gtest.h>
#include "resource_utils.h"

fs::path resourcePath;

int main(int argc, char** argv) {
    testing::InitGoogleTest(&argc, argv);
    if (argc < 2) {
        return -1;
    }
    resourcePath = argv[1];
    std::cout << "Path to data dir: " << argv[1] << std::endl;
    return RUN_ALL_TESTS();
}

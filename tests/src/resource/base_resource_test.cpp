#include "resource_utils.h"
#include "resource_holder.h"

TEST(BaseTest, Load) {
    Thelema::SoundsHolder holder;
    ASSERT_NO_THROW(holder.add((resourcePath / fs::path("sounds") / fs::path("sample.waw")).c_str(),
                               Thelema::Sounds::SoundsCnt));
}

TEST(BaseTest, Get) {
    Thelema::SoundsHolder holder;
    holder.add((resourcePath / fs::path("sounds") / fs::path("sample.waw")).c_str(),
               Thelema::Sounds::SoundsCnt);
    ASSERT_NO_THROW(holder.get(Thelema::Sounds::SoundsCnt));
}

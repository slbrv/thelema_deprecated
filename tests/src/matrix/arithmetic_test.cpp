#include "matrix.h"
#include "matrix_utils.h"

TEST(ArithmeticTest, Sum) {
    const auto matrix1 = get5x5Matrix<double>();
    const auto matrix2 = get5x5Matrix<double>();
    const auto resMatrix = matrix1 + matrix2;
    for (size_t i = 0; i < resMatrix.getRows(); ++i) {
        for (size_t j = 0; j < resMatrix.getCols(); ++j) {
            ASSERT_DOUBLE_EQ(matrix1(i, j) + matrix2(i, j), resMatrix(i, j));
        }
    }
}

TEST(ArithmeticTest, Sub) {
    const auto matrix1 = get5x5Matrix<double>();
    const auto matrix2 = get5x5Matrix<double>();
    const auto resMatrix = matrix1 - matrix2;
    for (size_t i = 0; i < resMatrix.getRows(); ++i) {
        for (size_t j = 0; j < resMatrix.getCols(); ++j) {
            ASSERT_DOUBLE_EQ(matrix1(i, j) + matrix2(i, j), resMatrix(i, j));
        }
    }
}

TEST(ArithmeticTest, ScalarMul) {
    const auto matrix1 = get5x5Matrix<double>();
    const auto val = getRandom<double>();
    const auto resMatrix = val * matrix1;
    for (size_t i = 0; i < resMatrix.getRows(); ++i) {
        for (size_t j = 0; j < resMatrix.getCols(); ++j) {
            ASSERT_DOUBLE_EQ(val * matrix1(i, j), resMatrix(i, j));
        }
    }
}

TEST(ArithmeticTest, MatrixMul) {
    const auto matrix1 = get5x5Matrix<double>();
    const auto matrix2 = get5x5Matrix<double>();
    const auto resMatrix = matrix1 * matrix2;
    ASSERT_EQ(matrix1.getRows(), resMatrix.getRows());
    ASSERT_EQ(matrix2.getCols(), resMatrix.getCols());

    for (size_t i = 0; i < resMatrix.getRows(); ++i) {
        for (size_t j = 0; j < resMatrix.getCols(); ++j) {
            double sum = 0.0;
            for (size_t k = 0; k < matrix1.getCols(); ++k) {
                sum += matrix1(i, k) * matrix2(k, j);
            }
            ASSERT_DOUBLE_EQ(sum, resMatrix(i, j));
        }
    }
}

#include <gtest/gtest.h>
#include "matrix_utils.h"
#include "exceptions.h"

TEST(MatrixOperationsTest, Transp) {
    const auto matrix = get5x5Matrix<double>();
    const auto resMatrix = matrix.transp();
    for (size_t i = 0; i < resMatrix.getRows(); ++i) {
        for (size_t j = 0; j < resMatrix.getCols(); ++j) {
            ASSERT_DOUBLE_EQ(resMatrix(i, j), matrix(j, i));
        }
    }
}

TEST(MatrixOperationsTest, Det) {
    std::ifstream in((matrixDataPath / fs::path("det") / fs::path("in")).c_str());
    const auto matrix = get5x5MatrixFromFile(in);
    double det = 0.0;
    in >> det;
    in.close();
    ASSERT_DOUBLE_EQ(det, matrix.det()); // TODO: near
}

TEST(MatrixOperationsTest, Adj) {
    std::ifstream in((matrixDataPath / fs::path("adj") / fs::path("in")).c_str());
    const auto matrix = get5x5MatrixFromFile(in);
    in.close();
    in.open((matrixDataPath / fs::path("adj") / fs::path("out")).c_str());
    const auto resMatrix = get5x5MatrixFromFile(in);
    in.close();
    ASSERT_EQ(matrix.adj(), resMatrix);
}

TEST(MatrixOperationsTest, Inv) {
    std::ifstream in((matrixDataPath / fs::path("inv") / fs::path("in")).c_str());
    const auto matrix = get5x5MatrixFromFile(in);
    in.close();
    in.open((matrixDataPath / fs::path("inv") / fs::path("out")).c_str());
    const auto resMatrix = get5x5MatrixFromFile(in);
    in.close();
    ASSERT_EQ(matrix.inv(), resMatrix);
}

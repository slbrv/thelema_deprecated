#include <gtest/gtest.h>
#include "matrix_utils.h"
#include "exceptions.h"


TEST(BaseTest, ConstructorAndBaseFuncions) {
    Thelema::Matrix<5, 5, float> matrix;
    ASSERT_EQ(matrix.getCols(), matrix.getRows());
    ASSERT_EQ(matrix.getCols(), 5);
    ASSERT_EQ(matrix.get(), true);
    ASSERT_NO_THROW(matrix(4, 4));
}

TEST(BaseTest, ConstructorCopy) {
    auto matrix1 = get5x5Matrix<float>();
    auto matrix2(matrix1);
    ASSERT_FALSE(matrix1.get() == matrix2.get());
    ASSERT_EQ(matrix1, matrix2);
}

TEST(BaseTest, ConstructorMove) {
    Thelema::Matrix<5, 5, float> matrix1;
    const auto ptr = matrix1.get();
    auto matrix2(std::move(matrix1));
    ASSERT_EQ(matrix2.get(), ptr);
    ASSERT_EQ(matrix1.get(), nullptr);
}

TEST(BaseTest, CopyAssignment) {
    auto matrix1 = get5x5Matrix<float>();
    Thelema::Matrix<5, 5, float> matrix2;
    matrix2 = matrix1;
    ASSERT_FALSE(matrix1.get() == matrix2.get());
    ASSERT_EQ(matrix1, matrix2);
}

TEST(BaseTest, MoveAssignment) {
    auto matrix1 = get5x5Matrix<float>();
    const auto ptr = matrix1.get();
    Thelema::Matrix<5, 5, float> matrix2;
    matrix2 = matrix1;
    ASSERT_EQ(matrix2.get(), ptr);
    ASSERT_EQ(matrix1.get(), nullptr);
}

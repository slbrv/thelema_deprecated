#include "matrix_utils.h"

Thelema::Matrix<5, 5, double> get5x5MatrixFromFile(std::ifstream &in) {
    Thelema::Matrix<5, 5, double> matrix;
    for (int i = 0; i < 5; ++i) {
        for (int j = 0; j < 5; ++j) {
            in >> matrix(i, j);
        }
    }
    return matrix;
};
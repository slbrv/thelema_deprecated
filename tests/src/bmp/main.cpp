#include "gtest/gtest.h"
#include "experimental/filesystem"

char *BmpImagePath;

int main(int argc, char** argv) {
    testing::InitGoogleTest(&argc, argv);
    if (argc < 2) {
        return -1;
    }
    BmpImagePath = argv[1];
    std::cout << "Path to image: " << argv[1] << std::endl;
    return RUN_ALL_TESTS();
}


#include "gtest/gtest.h"
#include "bmp_parser.h"
#include "stdio.h"

extern char *BmpImagePath;

TEST(BMP_PARSER_INITIALIZATION,correct_header_read){
    FILE* f=fopen(BmpImagePath,"rb");
    BMPImage Img(BmpImagePath);
    char Header[sizeof(BMPHeader)];
    fread(Header,sizeof(BMPHeader),1,f);
    ASSERT_EQ(Img.GetHeader(),Header);
}

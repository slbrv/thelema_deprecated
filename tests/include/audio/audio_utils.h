#ifndef THELEMA_ENGINE_AUDIO_UTILS_H
#define THELEMA_ENGINE_AUDIO_UTILS_H

#include <gtest/gtest.h>
#include <experimental/filesystem>
#include "audio.h"

namespace fs = std::experimental::filesystem;
extern fs::path resourcePath;

class BaseTest : public ::testing::Test {
protected:
    void SetUp() override;
    void TearDown() override;

    std::unique_ptr<Thelema::Audio> _audio;
    std::unique_ptr<Thelema::SoundsHolder> _soundHolder;
};



#endif //THELEMA_ENGINE_AUDIO_UTILS_H

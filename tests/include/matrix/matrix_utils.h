#ifndef THELEMA_ENGINE_RESOURCE_UTILS_H
#define THELEMA_ENGINE_UTILS_H
#include <gtest/gtest.h>
#include <experimental/filesystem>
#include <random>
#include <iostream>
#include <fstream>
#include "matrix.h"

namespace fs = std::experimental::filesystem;
extern fs::path matrixDataPath;

template<class T>
T getRandom(size_t min = 0, size_t max = 100) {
    static std::mt19937 gen(std::random_device{}());
    std::uniform_int_distribution<T> distribution(min, max);
    return distribution(gen);
}

template<class T>
auto get5x5Matrix() {
    Thelema::Matrix<5, 5, T> matrix;
    for (int i = 0; i < 5; ++i) {
        for (int j = 0; j < 5; ++j) {
            matrix(i, j) = getRandom<T>();
        }
    }
    return matrix;
}

Thelema::Matrix<5, 5, double> get5x5MatrixFromFile(std::ifstream &in);

template<class T>
class TestMatrix {
public:
    TestMatrix(size_t row = 0, size_t cols = 0) : _arr(new T[row * cols]), _rows(row), _cols(cols) {}

    TestMatrix(const TestMatrix &matrix) = delete;
    TestMatrix(TestMatrix &&matrix) = delete;
    TestMatrix &operator=(TestMatrix &&matrix) = delete;
    TestMatrix &operator=(const TestMatrix &matrix) = delete;

    T *getArr() const {
        return _arr;
    }

    T &getAt(size_t row, size_t col) {
        return _arr[_cols * row + col];
    }

    size_t getRows() const {
        return _rows;
    }

    size_t getCols() const {
        return _cols;
    }

    ~TestMatrix() {
        delete[] _arr;
    }

private:
    T *_arr;
    size_t _rows;
    size_t _cols;
};

#endif //THELEMA_ENGINE_RESOURCE_UTILS_H

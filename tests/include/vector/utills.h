//
// Created by nexachu on 14.04.2020.
//

#ifndef THELEMA_ENGINE_VECTOR_TESTS_H
#define THELEMA_ENGINE_VECTOR_TESTS_H

#include "gtest/gtest.h"
#include "experimental/filesystem"
#include "vec.h"
#include <random>

namespace fs=std::experimental::filesystem;
extern fs::path vectorData;

template<class T>
T GetRandom(size_t min = 0, size_t max = 100) {
static std::mt19937 gen(std::random_device{}());
std::uniform_int_distribution<T> distribution(min, max);
return distribution(gen);
}

template<class T>
auto GetVector(){
    Thelema::Vec<T,4> vec;
    for (int i=0;i<4;i++){
        vec(i)=GetRandom<T>();
    }
    return vec;
}
template<class T>
class TestVector {
public:
    TestVector (size_t n = 0);
    TestVector(const TestVector<T>& vect);
    TestVector &operator=(const TestVector vect);

    T *getVec() const {
        return _arr;
    }

    ~TestVector() {
        delete[] _arr;
    }

private:
    T *_arr;
};

template<class T>
TestVector<T>::TestVector(size_t n) : _arr(new T[n]) {}

#endif //THELEMA_ENGINE_VECTOR_TESTS_H

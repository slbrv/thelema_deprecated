

#ifndef THELEMA_ENGINE_VECTOR_TESTS_H
#define THELEMA_ENGINE_VECTOR_TESTS_H
#pragma once
#include "gtest/gtest.h"
#include "experimental/filesystem"
#include <cstdio>
#include "vec.h"
#include <random>
#include <chrono>


extern std::experimental::filesystem::path vectorData;
typedef std::chrono::high_resolution_clock clk;
clk::time_point beginning= clk::now();

float GetRandom(){
    clk::duration d=clk::now() - beginning;
    unsigned  seed=d.count();
    std::mt19937 gen(seed);
    return gen();
}

template<class T>
auto GetVector(){
    Thelema::Vec<T,4> vec;
    for (int i=0;i<4;i++){
        *vec[i]=GetRandom();
    }
    return vec;
}

Thelema::Vec<double,4> getVectorfromFile(){
    Thelema::Vec<double,4> vector1;
    FILE* data=std::fopen(vectorData.c_str(),"r");
    for (int i=0;i<4;i++) {
         fread(vector1[i],sizeof(double),1,data);
    }
    fclose(data);
    return vector1;
}

#endif //THELEMA_ENGINE_VECTOR_TESTS_H

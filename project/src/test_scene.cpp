//MIT License

//This file is part of Thelema Engine

//Copyright (c) 2020 Vyacheslav

//Permission is hereby granted, free of charge, to any person obtaining a copy
//of this software and associated documentation files (the "Software"), to deal
//in the Software without restriction, including without limitation the rights
//to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//copies of the Software, and to permit persons to whom the Software is
//furnished to do so, subject to the following conditions:

//The above copyright notice and this permission notice shall be included in all
//copies or substantial portions of the Software.

//THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//SOFTWARE.


#include <thelema/input/input_system.h>
#include <thelema/core/core.h>
#include <scenes/test_scene.h>

//Here the game begins

using namespace Thelema;

TestScene::TestScene(){}

TestScene::~TestScene(){}

void TestScene::init()
{
    Core::getInstance().getFileSystem();
}
void TestScene::start()
{
//    Core::getInstance().getGraphicsSystem()->gl46->Viewport(0, 0, 800, 640);
}
void TestScene::update()
{
    auto graphics = Core::getInstance().getGraphicsSystem();
    auto input = Core::getInstance().getInputSystem();

    if(input->isKeyHold(KeyCode::R))
        r = 1.0f;
    else
        r = 0.0f;
    if(input->isKeyDown(KeyCode::G))
        g = 1.0f;
    else
        g = 0.0f;
    if(input->isKeyHold(KeyCode::B))
        b = 1.0f;
    else
        b = 0.0f;

//    graphics->gl46->ClearColor(r, g, b, 1.0f);
//    graphics->gl46->Clear(GL46::COLOR_BUFFER_BIT | GL46::DEPTH_BUFFER_BIT);
}


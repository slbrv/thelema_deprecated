#ifndef THELEMA_ENGINE_OBJECT_H
#define THELEMA_ENGINE_OBJECT_H
#pragma once
#include "iostream"
#include "entity.h"
class Object:Entity{
public:
    Object();
    ~Object();
    size_t GetName(){return Name;};
    size_t GetDescription(){return Description;};
private:
    wchar_t Name;
    wchar_t Description;
};
#endif //THELEMA_ENGINE_OBJECT_H

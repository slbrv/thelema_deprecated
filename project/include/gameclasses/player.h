#ifndef THELEMA_ENGINE_PLAYER_H
#define THELEMA_ENGINE_PLAYER_H
#include "istream"
#include "stats.h"
#include "quest.h"
#include "equipment.h"
#include "entity.h"

class Player:Entity{
public:

    wchar_t GetNickname();
    Equipment* GetEqipment();

    void ATK();
    void DEF();
    void Move();

private:
    wchar_t Nickname;
    Stats* PlayerStats;
    Equipment* _arrEquipment;
    size_t EquipmentBuffer;
    Quest* CurrentQuests;
    size_t QuestBuffer;
};
#endif //THELEMA_ENGINE_PLAYER_H

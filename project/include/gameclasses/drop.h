#ifndef THELEMA_ENGINE_DROP_H
#define THELEMA_ENGINE_DROP_H

#pragma once
#include "istream"
class Drop{
public:
    Drop();
    ~Drop();


    size_t CalcChance(){};
    size_t GetId(){};
    size_t GetChance(){};

private:
    size_t DropId;
    size_t DropType;
    size_t DropChance;
};
#endif //THELEMA_ENGINE_DROP_H

#ifndef THELEMA_ENGINE_STATS_H
#define THELEMA_ENGINE_STATS_H

#include "iostream"
Class Stats{
public:
    size_t* GetStats();
    void CalcStats();
private:
    size_t stats[]
    size_t numberofstats;
};
#endif //THELEMA_ENGINE_STATS_H

#ifndef THELEMA_ENGINE_QUEST_H
#define THELEMA_ENGINE_QUEST_H

class Quest{
public:
    size_t GetId();
    void GiveRewards();
    void TakeItems();
    size_t GetCompletionStatus();
private:
    size_t QuestId;
    size_t CompletionStatus;
    size_t Progress;
};
#endif //THELEMA_ENGINE_QUEST_H

#ifndef THELEMA_ENGINE_EQUIPMENT_H
#define THELEMA_ENGINE_EQUIPMENT_H

#include "iostream"
class Equipment{
public:
    size_t DamageUpgrade();
    size_t DefenceUpgrade();
    size_t* StatsUpgrade();
private:
    size_t EquipmentId;
    size_t EquipmentType;
};
#endif //THELEMA_ENGINE_EQUIPMENT_H

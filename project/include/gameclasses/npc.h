#ifndef THELEMA_ENGINE_NPC_H
#define THELEMA_ENGINE_NPC_H

#pragma once
#include "iostream"
#include "player.h"
#include "drop.h"
#include "stats.h"
#include "entity.h"

class NPC:Entity{
public:
    NPC();
    ~NPC();

    size_t GetName(){return Name;};
    size_t GetDescription(){return Description;};
    size_t* GetQuests(){return Quests;};

    void GiveQuest(Player P){};
    Drop* GetDrops(){}

    void Move();
    void ATK();
    void DEF();

private:
    wchar_t Name;
    wchar_t Description;
    size_t QuestCount;
    size_t* Quests;
    Drop* _arrDrop;
};
#endif //THELEMA_ENGINE_NPC_H

#ifndef THELEMA_ENGINE_ENTITY_H
#define THELEMA_ENGINE_ENTITY_H
#pragma once
#include "iostream"
    class Entity{
    public:
    Entity();
    ~Entity();
    size_t GetId(){return Id;};
    size_t GetStatus(){return Status;};
    private:
    size_t Id;
    size_t Status;
};
#endif //THELEMA_ENGINE_ENTITY_H

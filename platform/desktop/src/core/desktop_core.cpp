//MIT License

//This file is part of Thelema Engine

//Copyright (c) 2020 Vyacheslav

//Permission is hereby granted, free of charge, to any person obtaining a copy
//of this software and associated documentation files (the "Software"), to deal
//in the Software without restriction, including without limitation the rights
//to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//copies of the Software, and to permit persons to whom the Software is
//furnished to do so, subject to the following conditions:

//The above copyright notice and this permission notice shall be included in all
//copies or substantial portions of the Software.

//THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//SOFTWARE.

#include <thelema/core/core.h>

#include <GLFW/glfw3.h>
#include <thelema/system/debug_system.h>
#include <desktop/graphics/desktop_graphics_system.h>
#include <desktop/audio/desktop_audio_system.h>
#include <desktop/data/desktop_file_system.h>
#include <desktop/input/desktop_input_system.h>
#include <desktop/graphics/desktop_window_system.h>

Thelema::Core::Core() :
    _initialized(false),
    _debugSystem(new DebugSystem),
    _windowSystem(new DesktopWindowSystem),
    _inputSystem(new DesktopInputSystem),
    _fileSystem(new DesktopFileSystem),
    _graphicsSystem(new DesktopGraphicsSystem),
    _audioSystem(new DesktopAudioSystem),
    _sceneSystem(new SceneSystem)
{

}

Thelema::Core& Thelema::Core::getInstance()
{
    static Core instance;
    return instance;
}

Thelema::Core::~Core()
{
    glfwTerminate();
    _initialized = false;
    _debugSystem->success(
            "Core",
            "Core was disposed");
}

bool Thelema::Core::init() noexcept
{
    if (glfwInit() &&
        _debugSystem->init() &&
        _windowSystem->init() &&
        _inputSystem->init() &&
        _sceneSystem->init())
    {
        _debugSystem->success("Core::init()", "Core was initialized successfully.");
        _initialized = true;
    }
    else
    {
        _debugSystem->error("Core::init()", "Core wasn't initialized(!glfwInit()).");
    }
    return _initialized;
}

void Thelema::Core::start() noexcept
{
    _debugSystem->start();
    _inputSystem->start();
    _fileSystem->start();
    _graphicsSystem->start();
    _audioSystem->start();
    _windowSystem->start();
    _sceneSystem->start();

    _debugSystem->success(
        "Core::start()",
        "Core was started");
    while (update());
}

bool Thelema::Core::update() noexcept
{
    bool run =
        _debugSystem->update() &&
            _inputSystem->update() &&
            _fileSystem->update() &&
            _graphicsSystem->update() &&
            _audioSystem->update() &&
            _sceneSystem->update() &&
            _windowSystem->update();

    return run;
}

std::shared_ptr<Thelema::DebugSystem> Thelema::Core::getDebugSystem()
{
    return _debugSystem;
}

std::shared_ptr<Thelema::IWindowSystem> Thelema::Core::getWindowSystem()
{
    return _windowSystem;
}

std::shared_ptr<Thelema::IInputSystem> Thelema::Core::getInputSystem()
{
    return _inputSystem;
}

std::shared_ptr<Thelema::SceneSystem> Thelema::Core::getSceneSystem()
{
    return _sceneSystem;
}

std::shared_ptr<Thelema::IFileSystem> Thelema::Core::getFileSystem()
{
    return _fileSystem;
}
std::shared_ptr<Thelema::IGraphicsSystem> Thelema::Core::getGraphicsSystem()
{
    return _graphicsSystem;
}
std::shared_ptr<Thelema::IAudioSystem> Thelema::Core::getAudioSystem()
{
    return _audioSystem;
}

bool Thelema::Core::isInitialized() const
{
    return _initialized;
}
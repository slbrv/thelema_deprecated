//MIT License

//This file is part of Thelema Engine

//Copyright (c) 2020 Vyacheslav

//Permission is hereby granted, free of charge, to any person obtaining a copy
//of this software and associated documentation files (the "Software"), to deal
//in the Software without restriction, including without limitation the rights
//to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//copies of the Software, and to permit persons to whom the Software is
//furnished to do so, subject to the following conditions:

//The above copyright notice and this permission notice shall be included in all
//copies or substantial portions of the Software.

//THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//SOFTWARE.

#include <iostream>

#include <thelema/system/debug_system.h>

Thelema::DebugSystem::DebugSystem()
{

}

Thelema::DebugSystem::~DebugSystem()
{
    info("DebugSystem",
            "System was disposed");
}

bool Thelema::DebugSystem::init()
{
    success("DebugSystem::init()",
        "Debug system was init'ed");
    return true;
}
bool Thelema::DebugSystem::start()
{
    success("DebugSystem::started()",
        "Debug system was started");
    return true;
}
bool Thelema::DebugSystem::update()
{

    return true;
}

void Thelema::DebugSystem::info(const std::string& tag, const std::string& message)
{
        std::cout << tag << ": " << message << std::endl;
}

void Thelema::DebugSystem::success(const std::string& tag, const std::string& message)
{
    std::cout << "\x1b[32m" << tag << ": " << message << "\x1b[0m" << std::endl;
}

void Thelema::DebugSystem::warning(const std::string& tag, const std::string& message)
{
    std::cout << "\x1b[33m" << tag << ": " << message << "\x1b[0m" << std::endl;
}

void Thelema::DebugSystem::error(const std::string& tag, const std::string& message)
{
    std::cout << "\x1b[31m" << tag << ": " << message << "\x1b[0m" << std::endl;
}
//MIT License

//This file is part of Thelema Engine

//Copyright (c) 2020 Vyacheslav

//Permission is hereby granted, free of charge, to any person obtaining a copy
//of this software and associated documentation files (the "Software"), to deal
//in the Software without restriction, including without limitation the rights
//to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//copies of the Software, and to permit persons to whom the Software is
//furnished to do so, subject to the following conditions:

//The above copyright notice and this permission notice shall be included in all
//copies or substantial portions of the Software.

//THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//SOFTWARE.


//MIT License

//This file is part of Thelema Engine

//Copyright (c) 2020 Vyacheslav

//Permission is hereby granted, free of charge, to any person obtaining a copy
//of this software and associated documentation files (the "Software"), to deal
//in the Software without restriction, including without limitation the rights
//to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//copies of the Software, and to permit persons to whom the Software is
//furnished to do so, subject to the following conditions:

//The above copyright notice and this permission notice shall be included in all
//copies or substantial portions of the Software.

//THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//SOFTWARE.

#include <GLFW/glfw3.h>
#include <thelema/system/debug_system.h>
#include <thelema/core/core.h>
#include <desktop/graphics/desktop_window_system.h>

Thelema::DesktopWindowSystem::DesktopWindowSystem()
        :
        _initialized(false),
        _active(false),
        _activeWindowIndex(0),
        _windows()
{

}

Thelema::DesktopWindowSystem::~DesktopWindowSystem()
{
    _active = false;
    _initialized = false;
    Thelema::Core::getInstance().getDebugSystem()->info(
            "DesktopWindowSystem",
            "System was disposed");
}

bool Thelema::DesktopWindowSystem::init()
{
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 4);
    _initialized = true;
    Thelema::Core::getInstance().getDebugSystem()->success(
            "DesktopWindowSystem::setActiveWindow()",
            "Window system was init'ed");
    return _initialized;
}

bool Thelema::DesktopWindowSystem::start()
{
    if (_windows.empty())
    {
        Core::getInstance().getDebugSystem()->error(
                "Desktop Window System",
                "The window isn't init'ed");
        return false;
    }
    for (const std::shared_ptr<Window>& window : _windows)
        window->start();
    Thelema::Core::getInstance().getDebugSystem()->success(
            "DesktopWindowSystem::setActiveWindow()",
            "Window system was started");
    return true;
}

//The window with the index 0 is the main. If it is closed, the core stops
bool Thelema::DesktopWindowSystem::update()
{
    for (const std::shared_ptr<Window>& window : _windows)
        window->update();

    _active = !glfwWindowShouldClose(static_cast<GLFWwindow*>(_windows.at(0)->getWindow()));
    return _active;
}

void Thelema::DesktopWindowSystem::addWindow(std::shared_ptr<Window>& window)
{
    _windows.push_back(std::move(window));
}

void Thelema::DesktopWindowSystem::setActiveWindow(uint64 index)
{
    if (index < _windows.size())
    {
        _active = true;
        _windows.at(_activeWindowIndex)->deactivate();
        _activeWindowIndex = index;
        _windows.at(_activeWindowIndex)->activate();
        glfwSwapInterval(1);
        Core::getInstance().getDebugSystem()->success(
                "DesktopWindowSystem::setActiveWindow()",
                "Window was activated(index: " + std::to_string(_activeWindowIndex) + ")");
    }
    else
        Thelema::Core::getInstance().getDebugSystem()->error(
                "DesktopWindowSystem::setActiveWindow()",
                "No window with index: " + std::to_string(index));
}

std::shared_ptr<Thelema::Window> Thelema::DesktopWindowSystem::getActiveWindow()
{
    if (_active)
        return _windows.at(_activeWindowIndex);

    Thelema::Core::getInstance().getDebugSystem()->error(
            "DesktopWindowSystem::setActiveWindow()",
            "The Window System isn't active");
    return nullptr;
}
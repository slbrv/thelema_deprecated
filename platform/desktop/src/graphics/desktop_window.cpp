//MIT License

//This file is part of Thelema Engine

//Copyright (c) 2020 Vyacheslav

//Permission is hereby granted, free of charge, to any person obtaining a copy
//of this software and associated documentation files (the "Software"), to deal
//in the Software without restriction, including without limitation the rights
//to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//copies of the Software, and to permit persons to whom the Software is
//furnished to do so, subject to the following conditions:

//The above copyright notice and this permission notice shall be included in all
//copies or substantial portions of the Software.

//THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//SOFTWARE.

#define GLFW_INCLUDE_NONE

#include <thelema/graphics/window.h>
#include <GLFW/glfw3.h>
#include <glad/glad.h>
#include <thelema/system/debug_system.h>

Thelema::Window::Window()
        :
        _initialized(false),
        _title("NULL"),
        _width(0),
        _height(0),
        _window(nullptr),
        _state(CLOSED)
{
}

Thelema::Window::Window(
        std::string&& title,
        const uint64& width,
        const uint64& height)
        :
        _initialized(false),
        _title(title),
        _width(width),
        _height(height),
        _window(nullptr),
        _state(CLOSED)
{
}

Thelema::Window::~Window()
{
    glfwDestroyWindow(static_cast<GLFWwindow*>(_window));
    _initialized = false;
    _state = CLOSED;
}

bool Thelema::Window::init()
{
    _initialized = _window = glfwCreateWindow(
            _width,
            _height,
            _title.c_str(),
            nullptr,
            nullptr
    );
    _state = _initialized ? ACTIVE : CLOSED;
    return _initialized;
}

//TODO Configure glfwSwapInterval
void Thelema::Window::start()
{

}

bool Thelema::Window::update()
{
    glfwSwapBuffers(static_cast<GLFWwindow*>(_window));
    return true;
}

void Thelema::Window::activate()
{
    auto* windowPtr = static_cast<GLFWwindow*>(_window);
    glfwMakeContextCurrent(windowPtr);
    glfwSetWindowUserPointer(windowPtr, this);
    gladLoadGLLoader((GLADloadproc)glfwGetProcAddress);
    _state = ACTIVE;
}

void Thelema::Window::deactivate()
{
    _state = INACTIVE;
}

void* Thelema::Window::getWindow()
{
    return _window;
}

Thelema::Window::State Thelema::Window::getState()
{
    return _state;
}
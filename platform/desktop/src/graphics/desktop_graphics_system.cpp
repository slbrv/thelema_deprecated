//MIT License

//This file is part of Thelema Engine

//Copyright (c) 2020 Vyacheslav

//Permission is hereby granted, free of charge, to any person obtaining a copy
//of this software and associated documentation files (the "Software"), to deal
//in the Software without restriction, including without limitation the rights
//to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//copies of the Software, and to permit persons to whom the Software is
//furnished to do so, subject to the following conditions:

//copies or substantial portions of the Software.

//THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//SOFTWARE.

#include <thelema/core/core.h>
#include <desktop/graphics/desktop_graphics_system.h>

Thelema::DesktopGraphicsSystem::DesktopGraphicsSystem()
{

}

Thelema::DesktopGraphicsSystem::~DesktopGraphicsSystem()
{
    Core::getInstance().getDebugSystem()->info(
            "DesktopGraphicsSystem",
            "System was disposed");
}

bool Thelema::DesktopGraphicsSystem::init()
{
    Core::getInstance().getDebugSystem()->success(
        "GraphicSystem::init()",
        "Graphic system was init'ed");
    return true;
}

bool Thelema::DesktopGraphicsSystem::start()
{
    Core::getInstance().getDebugSystem()->success(
        "GraphicSystem::start()",
        "Graphic system was started");
    return true;
}

bool Thelema::DesktopGraphicsSystem::update()
{

    return true;
}

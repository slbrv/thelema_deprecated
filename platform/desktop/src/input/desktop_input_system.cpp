//MIT License

//This file is part of Thelema Engine

//Copyright (c) 2020 Vyacheslav

//Permission is hereby granted, free of charge, to any person obtaining a copy
//of this software and associated documentation files (the "Software"), to deal
//in the Software without restriction, including without limitation the rights
//to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//copies of the Software, and to permit persons to whom the Software is
//furnished to do so, subject to the following conditions:

//The above copyright notice and this permission notice shall be included in all
//copies or substantial portions of the Software.

//THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//SOFTWARE.

#include <GLFW/glfw3.h>
#include <thelema/system/debug_system.h>
#include <thelema/core/core.h>
#include <desktop/input/desktop_input_system.h>

Thelema::KeyAction* Thelema::KeyProcessor::_keys = nullptr;

Thelema::DesktopInputSystem::DesktopInputSystem() :
    _keys(new KeyAction[350])
{
    KeyProcessor::setProcessedKeys(_keys);
}

Thelema::DesktopInputSystem::~DesktopInputSystem()
{
    Core::getInstance().getDebugSystem()->info("DesktopInputSystem", "System was disposed");
}

bool Thelema::DesktopInputSystem::init()
{
    Core::getInstance().getDebugSystem()->success(
        "DesktopInputSystem::init()",
        "Input system was init'ed");
    return true;
}
bool Thelema::DesktopInputSystem::start()
{
    glfwSetKeyCallback(
        static_cast<GLFWwindow*>(Core::getInstance().getWindowSystem()->getActiveWindow()->getWindow()),
        reinterpret_cast<GLFWkeyfun>(KeyProcessor::keyProcess));
    Core::getInstance().getDebugSystem()->success(
        "DesktopInputSystem::started()",
        "Input system was started");
    return true;
}
bool Thelema::DesktopInputSystem::update()
{
    glfwPollEvents();
    return true;
}

bool Thelema::DesktopInputSystem::isKeyDown(KeyCode keyCode)
{
    KeyAction& key = _keys[keyCode];
    if (key == KeyAction::DOWN)
    {
        key = KeyAction::HOLD;
        return true;
    }
    return false;
}

bool Thelema::DesktopInputSystem::isKeyUp(KeyCode keyCode)
{
    KeyAction& key = _keys[keyCode];
    if (key == KeyAction::UP)
    {
        key = KeyAction::RELEASE;
        return true;
    }
    return false;
}

bool Thelema::DesktopInputSystem::isKeyHold(KeyCode keyCode)
{
    return _keys[keyCode] == KeyAction::HOLD ||
           _keys[keyCode] == KeyAction::DOWN;
}

bool Thelema::DesktopInputSystem::isMouseButtonUp(MouseButton button)
{
    return true;
}

bool Thelema::DesktopInputSystem::isMouseButtonDown(MouseButton button)
{
    return true;
}

bool Thelema::DesktopInputSystem::isMouseButtonHold(MouseButton button)
{
    return true;
}

void Thelema::KeyProcessor::keyProcess(void* window, int key, int scancode, int action, int mods)
{
    switch (action)
    {
        using Action = Thelema::KeyAction;
    case GLFW_RELEASE:
        _keys[key] = Action::UP;
        break;
    case GLFW_PRESS:
        _keys[key] = Action::DOWN;
        break;
    case GLFW_REPEAT:
        _keys[key] = Action::HOLD;
        break;
    default:
        _keys[key] = Action::RELEASE;
        break;
    }
}

void Thelema::KeyProcessor::setProcessedKeys(Thelema::KeyAction* keys)
{
    _keys = keys;
}
//MIT License

//This file is part of Thelema Engine

//Copyright (c) 2020 Vyacheslav

//Permission is hereby granted, free of charge, to any person obtaining a copy
//of this software and associated documentation files (the "Software"), to deal
//in the Software without restriction, including without limitation the rights
//to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//copies of the Software, and to permit persons to whom the Software is
//furnished to do so, subject to the following conditions:

//The above copyright notice and this permission notice shall be included in all
//copies or substantial portions of the Software.

//THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//SOFTWARE.


#ifndef THELEMA_ENGINE_DESKTOP_INPUT_SYSTEM_H
#define THELEMA_ENGINE_DESKTOP_INPUT_SYSTEM_H

#include <thelema/input/input_system.h>

namespace Thelema
{
    class DesktopInputSystem : public IInputSystem
    {
    public:
        DesktopInputSystem();

        bool init() override;
        bool start() override;
        bool update() override;

        bool isKeyDown(KeyCode keyCode) override;
        bool isKeyUp(KeyCode keyCode) override;
        bool isKeyHold(KeyCode keyCode) override;

        bool isMouseButtonUp(MouseButton button) override;
        bool isMouseButtonDown(MouseButton button) override;
        bool isMouseButtonHold(MouseButton button) override;

        ~DesktopInputSystem() override;
    private:
        KeyAction* _keys;
    };

    class KeyProcessor
    {
    public:
        KeyProcessor() = delete;
        KeyProcessor(const KeyProcessor&) = delete;

        KeyProcessor& operator=(const KeyProcessor&) = delete;

        static void keyProcess(void* window, int key, int scancode, int action, int mods);
        static void setProcessedKeys(KeyAction*);
    private:
        static KeyAction* _keys;
    };

}

#endif //THELEMA_ENGINE_DESKTOP_INPUT_SYSTEM_H
